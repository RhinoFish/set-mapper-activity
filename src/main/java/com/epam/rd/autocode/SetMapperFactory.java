package com.epam.rd.autocode;


import com.epam.rd.autocode.domain.Employee;
import com.epam.rd.autocode.domain.FullName;
import com.epam.rd.autocode.domain.Position;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class SetMapperFactory {
    private class Pair{
        Employee employee;
        BigInteger managerID;

        Pair(Employee employee, BigInteger managerID){
            this.employee = employee;
            this.managerID = managerID;
        }

        @Override
        public String toString(){
            return employee.toString() + " Manager: " + managerID + "\n";
        }

    }
    public Employee getManager(Pair tempEmployee,HashMap<BigInteger,Pair> employeesMap){
        if(tempEmployee.managerID.intValue() ==  0 || employeesMap.get(tempEmployee.managerID) == null)
            return null;
        else
            return new Employee(employeesMap.get(tempEmployee.managerID).employee.getId(),
                                employeesMap.get(tempEmployee.managerID).employee.getFullName(),
                                employeesMap.get(tempEmployee.managerID).employee.getPosition(),
                                employeesMap.get(tempEmployee.managerID).employee.getHired(),
                                employeesMap.get(tempEmployee.managerID).employee.getSalary(),
                                getManager(employeesMap.get(tempEmployee.managerID),employeesMap));
    }

    public SetMapper<Set<Employee>> employeesSetMapper() {
        return resultSet -> {
            HashMap<BigInteger,Pair> employeesMap = new HashMap<>();
            Employee employee;
            Set<Employee> employees = new HashSet<>();
            try{

                while(resultSet.next()){
                    BigInteger ID = resultSet.getBigDecimal("ID").toBigInteger();
                    String firstName = resultSet.getString("FIRSTNAME");
                    String lastName = resultSet.getString("LASTNAME");
                    String middleName = resultSet.getString("MIDDLENAME");
                    Position position = Position.valueOf(resultSet.getString("POSITION"));
                    LocalDate hired = resultSet.getObject("HIREDATE",LocalDate.class);
                    BigDecimal salary = resultSet.getBigDecimal("SALARY");
                    Optional<BigDecimal> op = Optional.ofNullable(resultSet.getBigDecimal("MANAGER"));
                    BigInteger managerID = op.orElse(BigDecimal.valueOf(0)).toBigInteger();
                    FullName fullName = new FullName(firstName,lastName,middleName);
                    employee = new Employee(ID,fullName,position,hired,salary,null);
                    employeesMap.put(ID,new Pair(employee,managerID));
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            employeesMap.values().forEach(pair -> employees.add(new Employee(pair.employee.getId(),pair.employee.getFullName(),
                    pair.employee.getPosition(),pair.employee.getHired(),pair.employee.getSalary(),getManager(pair,employeesMap))));
            return employees;
        };
    }
}
